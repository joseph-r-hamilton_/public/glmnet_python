# -*- coding: utf-8 -*-
"""
def loadGlmLib():
=======================
INPUT ARGUMENTS:

                NONE

=======================
OUTPUT ARGUMENTS: 

glmlib          Returns a glmlib object with methods that are equivalent 
                to the fortran functions in GLMnet.f
=======================
"""
import ctypes
import os
import platform

system = platform.system()
glmnet_so = os.path.dirname(__file__) + '/GLMlib/' + system + '/GLMnet.so'

def loadGlmLib():
    try:
        glmlib = ctypes.cdll.LoadLibrary(glmnet_so)
        return(glmlib)
    except Exception as e:
        raise ValueError('loadGlmlib failed: ' + str(e))
    '''
    if os.name == 'posix':
        glmlib = ctypes.cdll.LoadLibrary(glmnet_so)
        return(glmlib)
    elif os.name == 'nt':
        # this does not currently work
        raise ValueError('loadGlmlib does not currently work for windows')
        # glmlib = ctypes.windll.LoadLibrary(glmnet_dll)
    else:
        raise ValueError('loadGlmLib not yet implemented for non-posix OS')
    '''
        
